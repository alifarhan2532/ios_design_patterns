//
//  LibraryAPI.h
//  BlueLibrary
//
//  Created by Ali Farhan on 15/01/2015.
//  Copyright (c) 2015 Eli Ganem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Album.h"
@interface LibraryAPI : NSObject
+ (LibraryAPI*)sharedInstance;
- (NSArray*)getAlbums;
- (void)addAlbum:(Album*)album atIndex:(int)index;
- (void)deleteAlbumAtIndex:(int)index;
- (void)saveAlbums;
@end
