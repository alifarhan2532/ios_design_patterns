//
//  AlbumView.h
//  BlueLibrary
//
//  Created by Ali Farhan on 15/01/2015.
//  Copyright (c) 2015 Eli Ganem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumView : UIView
- (id)initWithFrame:(CGRect)frame albumCover:(NSString*)albumCover;
@end
