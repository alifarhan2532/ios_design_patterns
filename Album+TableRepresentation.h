//
//  Album+TableRepresentation.h
//  BlueLibrary
//
//  Created by Ali Farhan on 15/01/2015.
//  Copyright (c) 2015 Eli Ganem. All rights reserved.
//

#import "Album.h"

@interface Album (TableRepresentation)
- (NSDictionary*)tr_tableRepresentation;
@end
